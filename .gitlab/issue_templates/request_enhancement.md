<!---
Please see filled-in example here: 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/examples/enhancement
--->

| Domain | Importance/Impact | s/w version | hardware | Accountable |
| :------: | :------: | :------: | :------: | :------: |
| Security/UI/service etc | High/Mid/Low | e.g. Oniro-1.0 | e.g. RPI4b | @gitlab_id |

## Domain
<!---
Please write below the domain(s) to which this request relates e.g. security, stability, UI, performance
--->

## Area
<!--- 
Please write below the specific area (if feasible) to which this request relates e.g. trust-chain, the doorlock blueprint, Zephyr's lvgl port
--->

## Impact / gain
<!---
What would be the impact or an improvement if such an enhancement would be provided?Please provide comprehensive information for easier follow-up and further processing
--->

## Enhancement description
<!--- 
Here is the placeholder for necessary technical details of such an enhancement
--->

<!--- Please don't edit after this point --->
/label ~"State::New"
/label ~"Type::Enhancement"
/draft
