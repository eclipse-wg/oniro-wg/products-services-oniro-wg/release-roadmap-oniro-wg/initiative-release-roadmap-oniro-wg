<!---
Please see filled-in example here: 
https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/roadmap-oniro-wg/wishlist-roadmap/-/wikis/examples/general_feedback
--->

| Domain | Importance/Impact | Accountable |
| :------: | :------: | :------: |
| e.g. s/w, h/w, organization, infra, security, ip compliance | High/Mid/Low | @gitlab_id |

## Summary
<!--- 
We really appreciate any feedback coming from the community. Thus, we strive to review and take it into account.

General, high level description for this feedback. 
For example: "In my opinion, the Oniro system / the community / the Zephyr's port of .. is .. because ..."
--->

## Details
<!--- 
More detailed description of above statement(s)
--->

## Expected actions
<!--- 
What actions are expected after this feedback?
--->

<!--- Please don't edit after this point --->
/label ~"State::New"
/label ~"Type::General"
/draft
